﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCuus
{
    public class Custom
    {
        private string Feedback = "";
        
        public void setFeedback(string feedback)
        {
            this.Feedback = feedback;
        }

        public string getFeedback()
        {
            return this.Feedback;
        }

    }
}