﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;
using F = System.IO;

namespace MVCuus.Controllers
{
    public class DepartmentsController : Controller
    {
        private SiimJaSEntities db = new SiimJaSEntities();

        //public static Dictionary<int, string> DepartmentName = new Dictionary<int, string>
        //{
        //    {1, "Admin" },
        //    {15, "Tootmisosakond" },
        //    {16, "Müügiosakond" },
        //    {21, "It osakond" },

        //};
        //[DisplayName("Osakond")]
        //public string DepartmentName => DepartmentName[this.DepartmentName];


        // GET: Departments

        public ActionResult Index()
         
        {

            var departments = db.Departments.Include(d => d.File);
            return View(departments.ToList());
        }

        // GET: Departments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
        }

        // GET: Departments/Create
        public ActionResult Create()
        {
            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename");
            return View();
        }

        // POST: Departments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DepartmentName,Picture")] Department department)
        {
            if (ModelState.IsValid)
            {
                db.Departments.Add(department);
                db.SaveChanges();
                return RedirectToAction("Index");
            }


            ViewBag.Picture = new SelectList(db.Files, "Id", "Filename", department.Picture);
            return View(department);
        }

        // GET: Departments/Edit/5
        public ActionResult Edit (int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }

            return View(department);
        }

        // POST: Departments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DepartmentName")] Department department, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                db.Entry(department).State = EntityState.Modified;
                db.Entry(department).Property(x => x.Picture).IsModified = false;
                db.SaveChanges();

                if (file != null && file.ContentLength > 0)
                {
                    using (F.BinaryReader br = new F.BinaryReader(file.InputStream))
                    {
                        if (department.Picture != null)
                        {
                            department.Picture = null;
                            db.SaveChanges();
                        }
                        byte[] buff = br.ReadBytes(file.ContentLength);
                        File uusFile = new File
                        {
                            Content = buff,
                            ContentType = file.ContentType,
                            Filename = file.FileName.Split('\\').Last()
                        };
                        db.Files.Add(uusFile);
                        db.SaveChanges();
                        department.Picture = uusFile.Id;
                        db.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }



            return View(department);
        }

        // GET: Departments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Department department = db.Departments.Find(id);
            if (department == null)
            {
                return HttpNotFound();
            }
            return View(department);
        }

        // POST: Departments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Department department = db.Departments.Find(id);
            db.Departments.Remove(department);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

}
