﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;


namespace MVCuus.Controllers
{
    public class TravelsController : Controller
    {
        private SiimJaSEntities db = new SiimJaSEntities();
    

       
        public ActionResult Index(string alates, string kuni, [Bind(Include = "Amount")] Travel travel)
        {
            // Valib poole aasta taguse aja, sest vastavalt nõuetele peab vaates nägema aruandeid alates poole aasta tagusest ajast. 
            TimeSpan poolAastatTagasi = new TimeSpan(4340, 0, 0); // tunnid, minutid, sekundid 4340
            DateTime tanaPoolAastatTagasi = DateTime.Today - poolAastatTagasi;

            DateTime dateAlates = DateTime.TryParse(alates, out DateTime da) ? da : tanaPoolAastatTagasi;
            DateTime dateKuni = DateTime.TryParse(kuni, out DateTime dk) ? dk : DateTime.Today;  //new DateTime(2020, 12, 31);

            ViewBag.alates = dateAlates;
            ViewBag.kuni = dateKuni;


            //var travels = db.Travels.Include(e => e.Person).ToList();
            var travels = db.Travels.Include(e => e.Person).ToList()
                    .Where(x => x.EndDate >= dateAlates)
                    .Where(x => x.EndDate <= dateKuni)
                    .ToList();

            if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Admin") ?? false))
                //Siin näidatakse kõiki aruandeid, mis ei ole staatusega 0 ehk mis ei ole pooleliolevad pluss
                //kõik raamatupidaja enda aruanded
                return View(travels.Where(x=>x.ReportState!=0).Union(db.Travels.Where(x => x.PersonId == db.People.Where(y => y.Email == User.Identity.Name).FirstOrDefault().Id)));
            else if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Ettevõtte juht") ?? false))
                //Siin näidatakse kõiki juhi all olevate töötajate aruandeid, mis ei ole staatusega 0 ehk mis ei ole pooleliolevad pluss
                //need juhi aruandeid, mis ei ole esitatud staatusega kuna nendega pole tal midagi teha, need on saadetud kinnitamisele raamatupidajale omakorda
                return View(travels.Where(x=>x.PersonId != db.People.Where(y => y.Email == User.Identity.Name).Single().Id).Where(x=>x.ReportState>0).
                Union(db.Travels.Where(x => x.PersonId == db.People.Where(y => y.Email == User.Identity.Name).FirstOrDefault().Id).Where(x => x.ReportState != 1).Where(x => x.ReportState != -1)));
            else if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Osakonna juht") ?? false))
                //Siin näidatakse alluvate aruandeid, mis on saadetud kinnitamisele ehk esitatud staatusega ja kinnitatud staatusega aruandeid pluss
                //osakonnajuhataja need aruanded, mis ei ole esitatud, kuna nendega pole tal midagi teha, need on saadetud ülevaatamisele oma otsesele juhile ja 
                //seejärel kinnitamisele raamatupidajale omakorda 
                return View(travels.Join(db.People.Where(y => y.DepartmentID == db.People.
                Where(z => z.Email == User.Identity.Name).FirstOrDefault().DepartmentID), x=>x.PersonId, z=>z.Id, (x,z)=>x).
                Where(x=>x.PersonId != db.People.Where(y => y.Email == User.Identity.Name).Single().Id).Where(x=>x.ReportState>0).
                Union(db.Travels.Where(x => x.PersonId == db.People.Where(y => y.Email == User.Identity.Name).FirstOrDefault().Id).Where(x=>x.ReportState!=1).Where(x => x.ReportState != -1)));
            else if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Tavakasutaja") ?? false))
                return View(travels.Where(x => x.PersonId == db.People.Where(y => y.Email == User.Identity.Name).Single().Id).Where(x=>x.ReportState!=-1).Where(x => x.ReportState != 1));
            else
                return RedirectToAction("Login", "Account");
        }

        private IEnumerable<Person> PeopleFilter()
        {
            //if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Osakonna juht") ?? false))
            //    return db.People.Where(y => y.DepartmentID == db.People.
            //    Where(z => z.Email == User.Identity.Name).FirstOrDefault().DepartmentID);
            //else if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Tavakasutaja") ?? false))
            //    return db.People.Where(y => y.Email == User.Identity.Name);
            //else return db.People;

            return db.People.Where(y => y.Email == User.Identity.Name);
        } 
        

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Admin") ?? false))

                return View();
            else return RedirectToAction("Index");
        }




        // GET: Travels/Details/5
        public ActionResult Details(int? id, string back = "")
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Travel travel = db.Travels.Find(id);
                ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                if (travel == null)
                {
                    return HttpNotFound();
                }

                Nullable<decimal> Summa = db.Spendings.Where(x => x.TravelId == travel.Id).Sum(x => x.Amount);
                ViewBag.Sum = Summa == null ? 0 : Summa;
                ViewBag.Back = back;
                return View(travel);
            }
            else return RedirectToAction("Login", "Account");
        }
        
        

        // GET: Travels/Create
        public ActionResult Create()
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.PersonId = new SelectList(PeopleFilter(), "Id", "FullName");
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");

                var travels = db.Travels.Include(t => t.Person);
                //Custom c = new Custom();

                return View();
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Travels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState, RepStatus,PersonId,TypeId,Amount,Document,Feedback")] Travel travel)
        {
            if (Request.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    db.Travels.Add(travel);
                    db.SaveChanges();

                    return RedirectToAction("Edit", new { id = travel.Id });
                }

                return View(travel);
            }
            else return RedirectToAction("Login", "Account");
        }

        // GET: Travels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");

                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Travel travel = db.Travels.Find(id);
                /*travel.Feedback = "niiteha"*/
                ;
                ViewBag.Feedback = travel.Feedback;
                //0 - Pooleli olev
                //1 - Esitatud
                //2 - Kinnitatud
                //-1 - Raamatupidaja käes
                if (travel.ReportState == 1)
                {
                    ViewBag.repState = "disabled";
                }
                if (travel.ReportState == 0 || travel.ReportState == 2)
                {
                    ViewBag.repConfirm = "disabled";
                }
                if (travel.ReportState == 2)
                {
                    ViewBag.repState = "disabled";
                }
                if (travel.ReportState == -1)
                {
                    ViewBag.repState = "disabled";
                }
                ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                if (travel == null)
                {
                    return HttpNotFound();
                }

                Nullable<decimal> Summa = db.Spendings.Where(x => x.TravelId == travel.Id).Sum(x => x.Amount);
                ViewBag.Sum = Summa == null ? 0 : Summa;

                return View(travel);
            }
            else return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Edit")]
        public ActionResult Edit([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,Feedback")] Travel travel)
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
                ViewBag.Feedback = travel.Feedback;

                if (ModelState.IsValid)
                {
                    db.Entry(travel).State = EntityState.Modified;
                    ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                return View(travel);
            }
            else return RedirectToAction("Login", "Account");
        }

        // GET: Travels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Travel travel = db.Travels.Find(id);
                ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                if (travel == null)
                {
                    return HttpNotFound();
                }

                Nullable<decimal> Summa = db.Spendings.Where(x => x.TravelId == travel.Id).Sum(x => x.Amount);
                ViewBag.Sum = Summa == null ? 0 : Summa;

                return View(travel);
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: Travels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            if (Request.IsAuthenticated)
            {
                Travel travel = db.Travels.Find(id);
                foreach (var x in db.Spendings.Where(y => y.TravelId == travel.Id))
                    db.Spendings.Remove(x);
                db.Travels.Remove(travel);

                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        [MultipleButton(Name = "action", Argument = "AddSpending")]
        public ActionResult AddSpending([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,TypeId,Amount,Document")] Travel travel)
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");

                if (ModelState.IsValid)
                {
                    if (travel.ReportState == 1)
                    {
                        ViewBag.repState = "disabled";
                    }
                    if (travel.ReportState == 0 || travel.ReportState == 2)
                    {
                        ViewBag.repConfirm = "disabled";
                    }
                    if (travel.ReportState == 2)
                    {
                        ViewBag.repState = "disabled";
                    }
                    if (travel.ReportState == -1)
                    {
                        ViewBag.repState = "disabled";
                    }

                    Spending spending = new Spending();
                    spending.TravelId = travel.Id;
                    Console.WriteLine(travel.Id);
                    spending.Amount = travel.Amount;
                    spending.Document = travel.Document;
                    spending.TypeId = travel.TypeId;


                    
                    db.Spendings.Add(spending);
                    db.SaveChanges();
                    Debug.WriteLine("SpenID: " + travel.Id);
                    ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                    ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
                    ViewBag.Sum = db.Spendings.Where(x => x.TravelId == travel.Id).Sum(x => x.Amount);


                    return View(travel);
                }

                return View();
            }
            else return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public ActionResult DeleteSpending(int id)
        {
            Debug.WriteLine("ID: " + id);
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            Spending spending = db.Spendings.Find(id);
            Travel travel = db.Travels.Find(spending.TravelId);
            //list1.get(0);
            //List<Spending> list = db.Spendings.Where(x => x.TravelId == travel.Id).ToList();
            //Spending i = list[0];
            if (ModelState.IsValid)
            {

                db.Spendings.Remove(spending);
                db.SaveChanges();
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
                ViewBag.Sum = db.Spendings.Where(x => x.TravelId == travel.Id).Sum(x => x.Amount);

                return View(travel);
            }
            return View(travel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "ForConfirm")]
        public ActionResult ForConfirm([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,Feedback")] Travel travel)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                travel.ReportState = 1;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(travel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "ForBooking")]
        public ActionResult ForBooking([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,Feedback")] Travel travel)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                travel.ReportState = -1;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(travel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "Confirm")]
        public ActionResult Confirm([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,Feedback")] Travel travel)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                travel.ReportState = 2;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(travel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [MultipleButton(Name = "action", Argument = "NotConfirm")]
        public ActionResult NotConfirm([Bind(Include = "Id,TravelName,StartDate,EndDate,Description,ReportState,RepStatus,PersonId,Feedback")] Travel travel)
        {
            ViewBag.PersonId = new SelectList(db.People, "Id", "FullName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            if (ModelState.IsValid)
            {
                db.Entry(travel).State = EntityState.Modified;
                ViewBag.sps = db.Spendings.Where(x => x.TravelId == travel.Id);
                travel.ReportState = 0;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(travel);
        }

        // POST: Travels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.

        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class MultipleButtonAttribute : ActionNameSelectorAttribute
    {
        public string Name { get; set; }
        public string Argument { get; set; }

        public override bool IsValidName(ControllerContext controllerContext, string actionName, MethodInfo methodInfo)
        {
            var isValidName = false;
            var keyValue = string.Format("{0}:{1}", Name, Argument);
            var value = controllerContext.Controller.ValueProvider.GetValue(keyValue);

            if (value != null)
            {
                controllerContext.Controller.ControllerContext.RouteData.Values[Name] = Argument;
                isValidName = true;
            }

            return isValidName;
        }

    }
}
