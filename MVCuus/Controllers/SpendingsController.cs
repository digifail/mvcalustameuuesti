﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;

namespace MVCuus.Controllers
{
    public class SpendingsController : Controller
    {
        private SiimJaSEntities db = new SiimJaSEntities();

        // GET: Spendings
        public ActionResult Index()
        {
            var spendings = db.Spendings.Include(s => s.Travel).Include(s => s.SpendingType);
            return View(spendings.ToList());
        }

        // GET: Spendings/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // GET: Spendings/Create
        public ActionResult Create()
        {
            ViewBag.Document = new SelectList(db.Files, "Id", "Filename");
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName");
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName");
            return View();
        }

        // POST: Spendings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelId,TypeId,Amount,Document")] Spending spending)
        {
            if (ModelState.IsValid)
            {
                db.Spendings.Add(spending);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // GET: Spendings/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // POST: Spendings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TravelId,TypeId,Amount,Document")] Spending spending)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spending).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Document = new SelectList(db.Files, "Id", "Filename", spending.Document);
            ViewBag.TravelId = new SelectList(db.Travels, "Id", "TravelName", spending.TravelId);
            ViewBag.TypeId = new SelectList(db.SpendingTypes, "Id", "SpendingName", spending.TypeId);
            return View(spending);
        }

        // GET: Spendings/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Spending spending = db.Spendings.Find(id);
            if (spending == null)
            {
                return HttpNotFound();
            }
            return View(spending);
        }

        // POST: Spendings/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Spending spending = db.Spendings.Find(id);
            db.Spendings.Remove(spending);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
