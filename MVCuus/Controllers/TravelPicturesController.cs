﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;

namespace MVCuus.Controllers
{
    public class TravelPicturesController : Controller
    {
        private SiimJaSEntities db = new SiimJaSEntities();

        // GET: TravelPictures
        public ActionResult Index()
        {
            var travelPictures = db.TravelPictures.Include(t => t.File).Include(t => t.Travel);
            return View(travelPictures.ToList());
        }

        // GET: TravelPictures/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelPicture travelPicture = db.TravelPictures.Find(id);
            if (travelPicture == null)
            {
                return HttpNotFound();
            }
            return View(travelPicture);
        }

        // GET: TravelPictures/Create
        public ActionResult Create()
        {
            ViewBag.FileId = new SelectList(db.Files, "Id", "Filename");
            ViewBag.TravelID = new SelectList(db.Travels, "Id", "TravelName");
            return View();
        }

        // POST: TravelPictures/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,TravelID,FileId,Description")] TravelPicture travelPicture)
        {
            if (ModelState.IsValid)
            {
                db.TravelPictures.Add(travelPicture);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.FileId = new SelectList(db.Files, "Id", "Filename", travelPicture.FileId);
            ViewBag.TravelID = new SelectList(db.Travels, "Id", "TravelName", travelPicture.TravelID);
            return View(travelPicture);
        }

        // GET: TravelPictures/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelPicture travelPicture = db.TravelPictures.Find(id);
            if (travelPicture == null)
            {
                return HttpNotFound();
            }
            ViewBag.FileId = new SelectList(db.Files, "Id", "Filename", travelPicture.FileId);
            ViewBag.TravelID = new SelectList(db.Travels, "Id", "TravelName", travelPicture.TravelID);
            return View(travelPicture);
        }

        // POST: TravelPictures/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TravelID,FileId,Description")] TravelPicture travelPicture)
        {
            if (ModelState.IsValid)
            {
                db.Entry(travelPicture).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.FileId = new SelectList(db.Files, "Id", "Filename", travelPicture.FileId);
            ViewBag.TravelID = new SelectList(db.Travels, "Id", "TravelName", travelPicture.TravelID);
            return View(travelPicture);
        }

        // GET: TravelPictures/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TravelPicture travelPicture = db.TravelPictures.Find(id);
            if (travelPicture == null)
            {
                return HttpNotFound();
            }
            return View(travelPicture);
        }

        // POST: TravelPictures/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TravelPicture travelPicture = db.TravelPictures.Find(id);
            db.TravelPictures.Remove(travelPicture);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
