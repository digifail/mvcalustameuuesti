﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;

namespace MVCuus.Controllers
{
    public class SpendingTypesController : Controller
    {
        private SiimJaSEntities db = new SiimJaSEntities();

        // GET: SpendingTypes
        public ActionResult Index()
        {
            return View(db.SpendingTypes.ToList());
        }

        // GET: SpendingTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }
            return View(spendingType);
        }

        // GET: SpendingTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SpendingTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,SpendingName")] SpendingType spendingType)
        {
            if (ModelState.IsValid)
            {
                db.SpendingTypes.Add(spendingType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(spendingType);
        }

        // GET: SpendingTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }
            return View(spendingType);
        }

        // POST: SpendingTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SpendingName")] SpendingType spendingType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(spendingType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(spendingType);
        }

        // GET: SpendingTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SpendingType spendingType = db.SpendingTypes.Find(id);
            if (spendingType == null)
            {
                return HttpNotFound();
            }
            return View(spendingType);
        }

        // POST: SpendingTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SpendingType spendingType = db.SpendingTypes.Find(id);
            db.SpendingTypes.Remove(spendingType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
