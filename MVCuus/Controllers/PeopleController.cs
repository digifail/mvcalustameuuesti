﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;



namespace MVCuus.Models
{
    partial class Person
    {

        public static Dictionary<int, string> States = new Dictionary<int, string>
        {
            {0, "ootel" },
            {1, "aktiivne" },
            {2, "puhkusel" },
            {3, "pensionil" },
            {4, "lahkunud" }
        };
        [DisplayName("Staatus")]
        public string State => States[this.PersonState];

        static SiimJaSEntities db = new SiimJaSEntities();

        public string FullName => FirstName + " " + LastName;

        public string FullNameIsikukoodiga => FirstName + " " + LastName + " " + PersonID;

        public static Person GetByEmail(string email)
            => db.People.Where(x => x.Email == email).SingleOrDefault();

        public bool IsInRole(string rolename)
            => this.UserInRoles
                .Select(x => x.Role.RoleName.ToLower()).ToList()
                .Intersect(rolename.Split(',').Select(y => y.Trim().ToLower()))
                .Count() > 0;

    }
    
}


namespace MVCuus.Controllers
{

    public class PeopleController : Controller
    {
        public SiimJaSEntities db = new SiimJaSEntities();



        // GET: People
        // Muutsin meetodit, et luua pagination funktsionaalsuse loomiseks vajalik
        // Lisasin ka sorteerimise võimaluse

        public ActionResult Index(string searchBy, string search, int page = 0, int size = 20, string sort = "PersonID", string direction = "ascending")
        {
            ViewBag.Page = page;
            ViewBag.Size = size;
            ViewBag.Pages = db.People.Count() / size;
            ViewBag.Sort = sort;
            ViewBag.Direction = direction;

            var sorted =
                 (direction == "descending" && sort == "FirstName") ? db.People.OrderByDescending(x => x.FirstName)
                : (sort == "FirstName") ? db.People.OrderBy(x => x.FirstName)
                : (direction == "descending" && sort == "LastName") ? db.People.OrderByDescending(x => x.LastName)
                : (sort == "LastName") ? db.People.OrderBy(x => x.LastName)
                : (direction == "descending") ? db.People.OrderByDescending(x => x.PersonID)
                : db.People.OrderBy(x => x.PersonID);

            if (Request.IsAuthenticated)
            {
                if (searchBy == "LastName")
                    return View(db.People.Where(x => x.LastName == search || search == null).ToList());
                if (searchBy == "FirstName")
                    return View(db.People.Where(x => x.FirstName.StartsWith(search) || search == null).ToList());
                else
                    return View(sorted.Skip(page * size).Take(size).ToList());

                //return View(sorted.Skip(page * size).Take(size).ToList()
                //    if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Admin") ?? false))
                //        return View(sorted.Skip(page * size).Take(size).ToList().Union(db.People.Where(x => x.LastName == search || search == null).ToList()));


            }
            return RedirectToAction("Login", "Account");

        }  
       

    public ActionResult AddRole(int? id, int? roleid)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }

            if (roleid == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Role role = db.Roles.Find(roleid);
            if (role == null)
            {
                return HttpNotFound();
            }

            try
            {
                db.UserInRoles.Add(new UserInRole { PersonId = id.Value, RoleId = roleid.Value });
                db.SaveChanges();
            }
            catch { }

            return RedirectToAction("Details", new { id = id });
        }

        public ActionResult RemoveRole(int? id, int? roleid)
        {
            Role r = db.Roles.Find(roleid ?? 0);
            Person p = db.People.Find(id ?? 0);
            if (p == null) return RedirectToAction("Index");
            if (r != null) 
            try
            {
                var uir = db.UserInRoles.Where(x => x.RoleId == roleid.Value && x.PersonId == id.Value).SingleOrDefault();
                if (uir != null) db.UserInRoles.Remove(uir);
                db.SaveChanges();
            }
            catch { }
            return RedirectToAction("Details", new { id = id.Value });
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                ViewBag.NewRoles = db.Roles.Where(x => !x.UserInRoles.Select(y => y.PersonId).ToList().Contains(id.Value));

                return View(person);
            }
            else return RedirectToAction("Login", "Account");
        }

        // GET: People/Create
        public ActionResult Create()
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName");
                ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
                ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");

                return View();
            }
            else return RedirectToAction("Login", "Account");
        }

        //ViewBag.PersonID = new SelectList(db.People, "Id", "FullName");
        //    return View();

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Lisatud HttpPostedFileBase file, mis on vajalik, et saaks pilti yles laadida
        public ActionResult Create([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person person, int RoleId, HttpPostedFileBase file) //siia lisasin int role id, et see salvestuks töötaja lisamise juures
        {
            if (Request.IsAuthenticated)
            {
                if (ModelState.IsValid) // && person.DepartmentID != 0)
                {
                    //See lõik on vajalik, et töötajate lisamis lehel saaks töötajale lisada ka rolli
                    Role r = db.Roles.Find(RoleId);
                    if (r != null) person.UserInRoles.Add(new UserInRole { Role = r });
                    db.People.Add(person);

                    db.SaveChanges();

                    return RedirectToAction("Index");

                }//Selle rea kopeerisin, et dropdownis ei saaks valida tühja lehte
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName");
                ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
                ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
                return View(person);
            }
            else return RedirectToAction("Login", "Account");
        }

        // Kogu see ActionResult on lisatud pildi lisamiseks
        // POST: People/AddPicture/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPicture([Bind(Include = "Id")]Person person0, HttpPostedFile pilt)
        {
            int id = person0.Id;
            Person person = db.People.Find(id);
            if (person == null) return RedirectToAction("Index");
            if (pilt == null || pilt.ContentLength == 0) return RedirectToAction("Details", new { id = id });
            File newFile = new File();
            using (System.IO.BinaryReader br = new System.IO.BinaryReader(pilt.InputStream))
            {
                byte[] buff = br.ReadBytes(pilt.ContentLength);
                newFile.Content = buff;
            }
            db.SaveChanges();
            person.Picture = newFile.Id;
            db.SaveChanges();


            return RedirectToAction("Details", new { id = id });
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                // Lisada hiljem
                //ViewBag.PersonState = new SelectList(Person.States, "Key", "Value", person.PersonState);

                // Saab valida departmenti inimesele
                // Id on Key ja DepartmentName on value
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName", person.Department);
                ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
                ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
                ViewBag.NewRoles = db.Roles.Where(x => !x.UserInRoles.Select(y => y.PersonId).ToList().Contains(id.Value));
                return View(person);
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,PersonID,FirstName,LastName,StartDate,PersonState,DepartmentID,Picture,Email")] Person Person) /* HttpPostedFileBase file)*/ //siia lisasin int role id, et see salvestuks töötaja lisamise juures)
        {
            if (Request.IsAuthenticated)
            {
                if (ModelState.IsValid) //&& person.DepartmentID != 0)
                {
                    db.Entry(Person).State = EntityState.Modified;

                    db.SaveChanges();

                    return RedirectToAction("Index");

                }//Selle rea kopeerisin, et dropdownis ei saaks valida tühja lehte
                ViewBag.DepartmentId = new SelectList(db.Departments, "Id", "DepartmentName");
                ViewBag.PersonState = new SelectList(Person.States, "Key", "Value");
                ViewBag.RoleId = new SelectList(db.Roles, "Id", "RoleName");
                return View(Person);
            }
            else return RedirectToAction("Login", "Account");
        }

        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(person).State = EntityState.Modified;
        //        // See rida on lisatud, et systeem ei hakkaks pilte nulliks keerama
        //        db.Entry(person).Property(x => x.Picture).IsModified = false;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(person);
        //}

        // GET: People/Delete/5






        public ActionResult Delete(int? id)
        {
            if (Request.IsAuthenticated)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Person person = db.People.Find(id);
                if (person == null)
                {
                    return HttpNotFound();
                }
                return View(person);
            }
            else return RedirectToAction("Login", "Account");
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
         
            Person person = db.People.Find(id);
            Travel travel = db.Travels.Find(id);
            foreach (var x in db.UserInRoles.Where(y => y.PersonId == person.Id ))   
            db.UserInRoles.Remove(x);
            db.SaveChanges();

            foreach (var reis in db.Travels.Where(x => x.PersonId == person.Id))
            { 
            foreach (var kulu in db.Spendings.Where(y => y.TravelId == reis.Id))    
            db.Spendings.Remove(kulu);
            db.Travels.Remove(reis);
            }        
            db.People.Remove(person);
            db.SaveChanges();
 
            return RedirectToAction("Index");

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
