﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCuus.Models;

namespace MVCuus.Controllers
{
    

    public class HomeController : Controller
    {
        SiimJaSEntities db = new SiimJaSEntities();

        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Travels");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        //    ViewBag.Kasutaja =
        //        Request.IsAuthenticated
        //        ?
        //            ViewBag.Kasutaja = Person.GetByEmail(User.Identity.Name)
        //            ?.FullName ?? "Võõras kasutaja"
        //        :
        //            "Tundmatu";
        //    ViewBag.Rollid = Request.IsAuthenticated ?
        //        string.Join(",", Person.GetByEmail(User.Identity.Name)?.UserInRoles.Select(x => x.Role.RoleName) ?? (new string[] { }))
        //        : "";

        //    return View();
        //}




        //public ActionResult Index()
        //{
        //    ViewBag.Kasutaja =
        //        Request.IsAuthenticated
        //        ?
        //            ViewBag.Kasutaja = Person.GetByEmail(User.Identity.Name)
        //            ?.FullName ?? "Võõras kasutaja"
        //        :
        //            "Tundmatu";
        //    ViewBag.Rollid = Request.IsAuthenticated ?
        //        string.Join(",", Person.GetByEmail(User.Identity.Name)?.UserInRoles.Select(x => x.Role.RoleName) ?? (new string[] { }))
        //        : "";

        //    return View();
        //}




        //public ActionResult About()
        //{
        //    ViewBag.Message = "Your application description page.";
        //    if (Request.IsAuthenticated && (Person.GetByEmail(User.Identity.Name)?.IsInRole("Autojuht,Admin") ?? false))

        //        return View();
        //    else return RedirectToAction("Index");
        //}


        //public ActionResult Contact()
        //{
        //    ViewBag.Message = "Your contact page.";

        //    return View();
        //}
    }
}